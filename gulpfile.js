const gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	jshintReporter = require('jshint-stylish'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	sassGlob = require('gulp-sass-glob'),
	browserify = require('gulp-browserify'),
	plumber = require('gulp-plumber'),
	babel = require('gulp-babel'),
	uglify = require('gulp-uglify'),
	sassLint = require('gulp-sass-lint'),
	autoprefixer = require('gulp-autoprefixer'),
	webserver = require('gulp-webserver'),
	nunjucks = require('gulp-nunjucks-render');

// ------------------------------------------
// Globals
// ------------------------------------------
var paths = {
	scripts: ['./scripts/**/*.js'],
	images: ['./images/**/*.jpg', './images/**/*.png', './images/**/*.gif', './images/**/*.svg'],
	scss: './sass/**/*.scss'
}

// ------------------------------------------
// Local Server
// ------------------------------------------
gulp.task('startServer', function() {
	gulp.src('public')
		.pipe(webserver({
			livereload: true,
			open: true,
			port: 8888
		}));
});

// ------------------------------------------
// Browswerify - JS
// ------------------------------------------
gulp.task('browserify', function() {
	gulp.src('./script/app.js')
		.pipe(plumber())
		.pipe(browserify({
			insertGlobals : true
		}))
		.pipe(gulp.dest('./public/assets/js/bundle'))
});

// ------------------------------------------
// Templating - Nunjucks
// ------------------------------------------
gulp.task('templates', function () {
	var nunjucksConf = {
		path: 'templates',
		envOptions: {
		tags: {
			blockStart: '{%',
			blockEnd: '%}',
			variableStart: '{$',
			variableEnd: '$}',
			commentStart: '{#',
			commentEnd: '#}'
			}
		}
	}

	gulp.src(['templates/*.html', '!templates/index.html'])
		.pipe(nunjucks(nunjucksConf))
		.pipe(gulp.dest('public/'));

	gulp.src('templates/index.html')
		.pipe(nunjucks(nunjucksConf))
		.pipe(gulp.dest('public/', {overwrite: true}));
});

// ------------------------------------------
// Babelify, Uglify, Minify - Js
// ------------------------------------------
gulp.task('uglifyJs', () => { //Uglifies and transpiles
	return gulp.src('public/assets/js/bundle/app.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(uglify())
		.pipe(gulp.dest('./public/assets/js/bundle'));
});

// ------------------------------------------
// Complie SASS
// ------------------------------------------
gulp.task('sass', function() {
	return gulp.src(paths.scss)
		.pipe(sassGlob())
		.pipe(sass().on('error', sass.logError))
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(autoprefixer())
		.pipe(gulp.dest('./public/assets/css/'))
});

// ------------------------------------------
// Linting - Js CSS
// ------------------------------------------
gulp.task('sasslint', function () {
	gulp.src('./sass/**/*.scss')
		.pipe(sassLint())
		.pipe(sassLint.format())
		.pipe(sassLint.failOnErrolkr())
});

// ------------------------------------------
// Bundled tasks - watching / testing etc
// ------------------------------------------

// Prep of assets - ugilify, minify, Babel Transpile
gulp.task('serve', ['startServer', 'templates', 'sass', 'browserify'], function() {
		gulp.watch("./sass/**/*.scss", ['sass']);
		gulp.watch("./script/**/*.js", ['browserify']);
		gulp.watch("./templates/**/*.html", ['templates']);
});

gulp.task('test', ['sasslint'	]);
gulp.task('build', ['uglifyJs']);
gulp.task('default', ['serve']);





